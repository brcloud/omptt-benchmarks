#include <math.h>
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>

#define ALPHA 0.25 // parameter of the filter

void read_image_header(int *w, int *h, int *n_grey) {
  scanf("%*[^\n]"); // skips the image type
  scanf("%d %d", w, h);
  scanf("%d", n_grey);
}

double *read_image_data(int w, int h, int n_grey) {
  int aux;
  double *ret = (double *)malloc(w * h * sizeof(double));
  for (int i = 0; i < h; i++)
    for (int j = 0; j < w; j++) {
      scanf("%d", &aux);
      ret[i * w + j] = (double)aux / n_grey;
    }

  return ret;
}

void print_image_header(int w, int h, int n_grey) {
  printf("P2\n");
  printf("%d %d\n", w, h);
  printf("%d\n", n_grey);
}

void print_image_data(double *imgOut, int w, int h, int n_grey, double max) {
  double gray_scale_value;
  for (int i = 0; i < h; i++) {
    for (int j = 0; j < w; j++) {
      gray_scale_value = fabs(imgOut[i * w + j] / max * n_grey);
      printf("%d ", (int)round(gray_scale_value));
      // printf("%.2f ", imgOut[i * w + j]); // debug
    }
    printf("\n");
  }
}

double matrix_abs_max(double *mat, int w, int h) {
  double max = 0;
  for (int i = 0; i < h; i++)
    for (int j = 0; j < w; j++)
      if (fabs(mat[i * w + j]) > max)
        max = fabs(mat[i * w + j]);
  return max;
}

/* Original code provided by Gael Deest */
double *deriche(double *imgIn, int w, int h) {
  double *ret = (double *)malloc(w * h * sizeof(double));
  double *y1, *y2;

  double xm1, tm1, ym1, ym2;
  double xp1, xp2;
  double tp1, tp2;
  double yp1, yp2;

  double k;
  double a1, a2, a3, a4, a5, a6, a7, a8;
  double b1, b2, c1, c2;

  k = (1.0 - exp(-ALPHA)) * (1.0 - exp(-ALPHA)) /
      (1.0 + 2.0 * ALPHA * exp(-ALPHA) - exp(2.0 * ALPHA));
  a1 = a5 = k;
  a2 = a6 = k * exp(-ALPHA) * (ALPHA - 1.0);
  a3 = a7 = k * exp(-ALPHA) * (ALPHA + 1.0);
  a4 = a8 = -k * exp(-2.0 * ALPHA);
  b1 = pow(2.0, -ALPHA);
  b2 = -exp(-2.0 * ALPHA);
  c1 = c2 = 1;

  int D_CHUNK = w;
#pragma omp parallel num_threads(2)
#pragma omp single
#pragma omp target data map(alloc : ret[0:w * h], y1[0:w * h], y2[0:w * h])    \
    map(to : imgIn[0:w * h])
  {
#pragma omp target depend(out : y1) nowait
#pragma omp teams num_teams(100)
#pragma omp distribute parallel for dist_schedule(static, D_CHUNK)             \
    schedule(static, 1) collapse(2)
    for (int i = ym1 = ym2 = xm1 = 0; i < h; i++)
      for (int j = 0; j < w; j++) {
        y1[i * w + j] = a1 * imgIn[i * w + j] + a2 * xm1 + b1 * ym1 + b2 * ym2;
        xm1 = imgIn[i * w + j];
        ym2 = ym1;
        ym1 = y1[i * w + j];
      }

#pragma omp target depend(out : y2) nowait
#pragma omp teams num_teams(100)
#pragma omp distribute parallel for dist_schedule(static, D_CHUNK)             \
    schedule(static, 1) collapse(2)
    for (int i = yp1 = yp2 = xp1 = xp2 = 0; i < h; i++)
      for (int j = w - 1; j >= 0; j--) {
        y2[i * w + j] = a3 * xp1 + a4 * xp2 + b1 * yp1 + b2 * yp2;
        xp2 = xp1;
        xp1 = imgIn[i * w + j];
        yp2 = yp1;
        yp1 = y2[i * w + j];
      }

#pragma omp target depend(in : y1, y2) depend(out : ret) nowait
#pragma omp teams num_teams(100)
#pragma omp distribute parallel for dist_schedule(static, D_CHUNK)             \
    schedule(static, 1) collapse(2)
    for (int i = 0; i < h; i++)
      for (int j = 0; j < w; j++)
        ret[i * w + j] = c1 * (y1[i * w + j] + y2[i * w + j]);

#pragma omp target depend(in : ret) depend(out : y1) nowait
#pragma omp teams num_teams(100)
#pragma omp distribute parallel for dist_schedule(static, D_CHUNK)             \
    schedule(static, 1) collapse(2)
    for (int j = tm1 = ym1 = ym2 = 0; j < w; j++)
      for (int i = 0; i < h; i++) {
        y1[i * w + j] = a5 * ret[i * w + j] + a6 * tm1 + b1 * ym1 + b2 * ym2;
        tm1 = ret[i * w + j];
        ym2 = ym1;
        ym1 = y1[i * w + j];
      }

#pragma omp target depend(in : ret) depend(out : y2) nowait
#pragma omp teams num_teams(100)
#pragma omp distribute parallel for dist_schedule(static, D_CHUNK)             \
    schedule(static, 1) collapse(2)
    for (int j = tp1 = tp2 = yp1 = yp2 = 0; j < w; j++)
      for (int i = h - 1; i >= 0; i--) {
        y2[i * w + j] = a7 * tp1 + a8 * tp2 + b1 * yp1 + b2 * yp2;
        tp2 = tp1;
        tp1 = ret[i * w + j];
        yp2 = yp1;
        yp1 = y2[i * w + j];
      }

#pragma omp target depend(in : y1, y2) depend(out : ret) nowait
#pragma omp teams num_teams(100)
#pragma omp distribute parallel for dist_schedule(static, D_CHUNK)             \
    schedule(static, 1) collapse(2)
    for (int i = 0; i < h; i++)
      for (int j = 0; j < w; j++)
        ret[i * w + j] = c2 * (y1[i * w + j] + y2[i * w + j]);

#pragma omp target update from(ret[0:w * h]) depend(in : ret)
  }

  return ret;
}

int main(int argc, char **argv) {
  freopen(argv[1], "rb", stdin); // reads the input from a file

  int w, h, n_grey;
  double *imgIn, *imgOut, max;

  read_image_header(&w, &h, &n_grey);
  imgIn = read_image_data(w, h, n_grey);

  imgOut = deriche(imgIn, w, h);

  print_image_header(w, h, n_grey);
  max = matrix_abs_max(imgOut, w, h);
  print_image_data(imgOut, w, h, n_grey, max);

  free(imgIn);
  free(imgOut);

  return 0;
}
