#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <omp.h>

#define SIZE 100
#define STEPS 50
#define SPACE 5 // MUST BE GREATER THAN 1 !!!

#define EPSILON 0.0000001


void print_speedup(double serial_start, double serial_end, double par_start, double par_end){
    printf("speedup: %.2lf\n", (serial_end - serial_start) / (par_end - par_start));
}

// supports mod of negative numbers
long int mod(long int a, long int b)
{
    long int r = a % b;
    return r < 0 ? r + b : r;
}

// do a index calculation in a linearized 3D matrix
long idx(long int i, long int j, long int k){
  return ((i * SIZE * SIZE) + (j * SIZE) + k);
}


void compare_matrices_2d(float **a, float **b, long int n){
  long int count = 0;
  for(int i = 0; i < n; i++){
    for(long int j = 0; j < n; j++){
      if (fabs(a[i][j] - b[i][j]) > EPSILON){
        // printf("%d\n", i); // DEBUG
        count++;
      }
    }
  }
  printf("Number of non-matching elements: %ld\n", count);
}

void print_array_2d(float **a, int n){
	for (int i = 0; i < n; i++){
    for (int j = 0; j < n; j++){
		    printf("%.2f ", a[i][j]);
    }
    printf("\n");
	}
	printf("\n");
}

int main(int argc, char *argv[]){

  //////////////////////// SERIAL ////////////////////////

  double s_start, s_end;
  s_start = omp_get_wtime();

	// allocate arrays
	float **A = (float**) malloc(sizeof(float*) * SIZE);
	float **B = (float**) malloc(sizeof(float*) * SIZE);

  for (int i = 0; i < SIZE; i++){
    A[i] = (float*) malloc(sizeof(float) * SIZE);
    B[i] = (float*) malloc(sizeof(float) * SIZE);
  }

	// initializing arrays w/ input
	for (long int i = 0; i < SIZE; i++){
    for (long int j = 0; j < SIZE; j++){
		  A[i][j] = sqrt(i * SIZE + j);
		  B[i][j] = 0;
    }
	}

	// stencil
  long int t, i, j;
	for (t = 0; t < STEPS; t++){

    // calc
    for (i = 1; i < SIZE - 1; i++)
      for (j = 1; j < SIZE - 1; j++)
        B[i][j] = 0.2 * (A[i][j] + A[i][j-1] + A[i][j + 1] + A[i + 1][j] + A[i-1][j]);

    // copy back
    for (i = 1; i < SIZE - 1; i++)
      for (j = 1; j < SIZE - 1; j++)
        A[i][j] = B[i][j];

	}

	s_end = omp_get_wtime();

	//////////////////////// PARALLEL ////////////////////////

	printf("Starting Parallel\n");
	double p_start, p_end;
	p_start = omp_get_wtime();

	// memory allocation
	float ***C = (float***) malloc(sizeof(float**) * SPACE);          // cube

	for (long int i = 0; i < SPACE; i++){
		C[i] = (float**) calloc(SIZE, sizeof(float*));                  // squares
    for (long int j = 0; j < SIZE; j++){
      C[i][j] = (float*) calloc(SIZE, sizeof(float));                 // lines
    }

	}

	// c[0] <- input
	for (long int i = 0; i < SIZE; i++)
    for (long int j = 0; j < SIZE; j++)
		  C[0][i][j] = sqrt(i * SIZE + j);

  // padding
  for (long int t = 1; t < SPACE; t++){
    for (long int i = 0; i < SIZE; i++){
      C[t][0][i] = C[0][0][i];                  // first row
      C[t][SIZE - 1][i] = C[0][SIZE - 1][i];    // last row
      C[t][i][0] = C[0][i][0];                  // first column
      C[t][i][SIZE - 1] = C[0][i][SIZE - 1];    // last column
    }
  }

	#pragma omp parallel num_threads(4) default(none) shared(C)
	{
		#pragma omp single
    {
      // stencil scheduler
      long int out_idx, in_idx;
      for (long int t = 1; t <= STEPS; t++){

        out_idx = mod(t, SPACE);
        in_idx = mod(t - 1, SPACE);

        for (long int i = 1; i < SIZE - 1; i++)
          for (long int j = 1; j < SIZE - 1; j++)
            #pragma omp task default(none) shared(C) \
                             firstprivate(out_idx, in_idx, i, j) \
                             depend(out: C[out_idx][i][j]) \
                             depend(in: C[in_idx][i][j]) \
                             depend(in: C[in_idx][i][j-1]) \
                             depend(in: C[in_idx][i][j + 1]) \
                             depend(in: C[in_idx][i + 1][j]) \
                             depend(in: C[in_idx][i-1][j])
              C[out_idx][i][j] = 0.2 * (C[in_idx][i][j]
                                      + C[in_idx][i][j-1]
                                      + C[in_idx][i][j + 1]
                                      + C[in_idx][i + 1][j]
                                      + C[in_idx][i-1][j]);


      }
		} // single
	} // parallel

	p_end = omp_get_wtime();

	//////////////////////// OUTPUT ////////////////////////

	compare_matrices_2d(A, C[STEPS % SPACE], SIZE);
	print_speedup(s_start, s_end, p_start, p_end);

  // deallocate 2D arrays
  for (i = 0; i < SIZE; i++){
    free(A[i]);
    free(B[i]);
  }
  free(A);
  free(B);

  // deallocate 3D arrays
  for (i = 0; i < SPACE; i++){
    for (j = 0; j < SIZE; j++){
      free(C[i][j]);
    }
    free(C[i]);
  }
  free(C);

	return 0;
}
