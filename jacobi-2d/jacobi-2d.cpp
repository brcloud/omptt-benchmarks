#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <omp.h>

#define SIZE 10
#define STEPS 5
#define SPACE 5 // MUST BE GREATER THAN 1 !!!
#define EPSILON 0.0000001


void print_speedup(double serial_start, double serial_end, double par_start, double par_end){
    printf("speedup: %.2lf\n", (serial_end - serial_start) / (par_end - par_start));
}

// supports mod of negative numbers
long int mod(long int a, long int b)
{
    long int r = a % b;
    return r < 0 ? r + b : r;
}

// do a index calculation in a linearized 3D matrix
long idx(long int i, long int j, long int k){
  return ((i * SIZE * SIZE) + (j * SIZE) + k);
}

// overloading to work with array of array
void compare_matrices_(float **a, float *c){
  long int count = 0;
  long int idx = SIZE * SIZE * (STEPS % SPACE);     // index where the output begins inside linear matrix c

  for(int i = 0; i < SIZE; i++){
    for(long int j = 0; j < SIZE; j++){
      if (fabs(a[i][j] - c[idx++]) > EPSILON){
        count++;
      }
    }
  }
  printf("Number of non-matching elements: %ld\n", count);
}

// overloading to work with array of array
void print_array_(float **a, int n){
	for (int i = 0; i < n; i++){
    for (int j = 0; j < n; j++){
		    printf("%.2f ", a[i][j]);
    }
    printf("\n");
	}
	printf("\n");
}

int main(int argc, char *argv[]){

  //////////////////////// SERIAL ////////////////////////

  double s_start, s_end;
  s_start = omp_get_wtime();

	// allocate arrays
	float **A = (float**) malloc(sizeof(float*) * SIZE);
	float **B = (float**) malloc(sizeof(float*) * SIZE);

  for (int i = 0; i < SIZE; i++){
    A[i] = (float*) malloc(sizeof(float) * SIZE);
    B[i] = (float*) malloc(sizeof(float) * SIZE);
  }

	// intializing arrays w/ input
	for (long int i = 0; i < SIZE; i++){
    for (long int j = 0; j < SIZE; j++){
		  A[i][j] = sqrt(i * SIZE + j);
		  B[i][j] = 0;
    }
	}

	// stencil
  long int t, i, j;
	for (t = 0; t < STEPS; t++){

    // calc
    for (i = 1; i < SIZE - 1; i++)
      for (j = 1; j < SIZE - 1; j++)
        B[i][j] = 0.2 * (A[i][j] + A[i][j-1] + A[i][j + 1] + A[i + 1][j] + A[i-1][j]);

    // copy back
    for (i = 1; i < SIZE - 1; i++)
      for (j = 1; j < SIZE - 1; j++)
        A[i][j] = B[i][j];

	}

	s_end = omp_get_wtime();

	//////////////////////// PARALLEL ////////////////////////

	printf("Starting Parallel\n");
	double p_start, p_end;
	p_start = omp_get_wtime();

  // memory alloc (linear)
  float *c = (float*) malloc(sizeof(float) * SIZE * SIZE * SPACE);

  // c[0][i][j] <- input
  for (long int i = 0; i < SIZE; i++)
    for (long int j = 0; j < SIZE; j++)
      c[idx(0, i, j)] = sqrt(i * SIZE + j);

  // padding (linear)
  for (long int t = 1; t < SPACE; t++){
    for (long int i = 0; i < SIZE; i++){
      c[idx(t, 0, i)] = c[idx(0, 0, i)]; // first row
      c[idx(t, SIZE - 1, i)] = c[idx(0, SIZE - 1, i)]; // first row
      c[idx(t, i, 0)] = c[idx(0, i, 0)]; // first column
      c[idx(t, i, SIZE - 1)] = c[idx(0, i, SIZE - 1)]; // last column
    }
  }

  #pragma omp target data map(tofrom: c[:SIZE * SIZE * SPACE])
  {

    // stencil scheduler
    long int out_idx, in_idx;
    for (long int t = 1; t <= STEPS; t++){

      out_idx = mod(t, SPACE);
      in_idx = mod(t - 1, SPACE);

      for (long int i = 1; i < SIZE - 1; i++)
        for (long int j = 1; j < SIZE - 1; j++)
          #pragma omp target nowait \
                           firstprivate(out_idx, in_idx, i, j) \
                           depend(out: c[idx(out_idx, i, j)]) \
                           depend(in: c[idx(in_idx, i, j)]) \
                           depend(in: c[idx(in_idx, i, j-1)]) \
                           depend(in: c[idx(in_idx, i, j+1)]) \
                           depend(in: c[idx(in_idx, i+1, j)]) \
                           depend(in: c[idx(in_idx, i-1, j)])
            c[idx(out_idx, i, j)] = 0.2 * (c[idx(in_idx, i, j)] +
                                           c[idx(in_idx, i, j-1)] +
                                           c[idx(in_idx, i, j+1)] +
                                           c[idx(in_idx, i+1, j)] +
                                           c[idx(in_idx, i-1, j)]);
    }
  } // data

	p_end = omp_get_wtime();

	//////////////////////// OUTPUT ////////////////////////

	compare_matrices_(A, c);
	print_speedup(s_start, s_end, p_start, p_end);

  // deallocate 2D arrays
  for (i = 0; i < SIZE; i++){
    free(A[i]);
    free(B[i]);
  }
  free(A);
  free(B);

  // deallocate linear matrix
  free(c);

	return 0;
}
