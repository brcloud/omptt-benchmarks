
#include <stdlib.h>

#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <unistd.h>

#include <vector>

template <typename T>
std::vector<std::vector<T>> makeMatrix(std::size_t rows, std::size_t cols) {
  return std::vector<std::vector<T>>(rows, std::vector<T>(cols));
}

template <typename T>
std::vector<std::vector<T>> makeMatrix(std::size_t rows, std::size_t cols,
                                       T value) {
  return std::vector<std::vector<T>>(rows, std::vector<T>(cols, value));
}

class BlockMatrix {
private:
  const int rowsPerBlock;
  const int colsPerBlock;
  const long nRows;
  const long nCols;
  const int nBlocksPerRow;
  const int nBlocksPerCol;

  std::vector<std::vector<float *>> Blocks;

public:
  BlockMatrix(const int _rowsPerBlock, const int _colsPerBlock,
              const long _nRows, const long _nCols)
      : rowsPerBlock(_rowsPerBlock), colsPerBlock(_colsPerBlock), nRows(_nRows),
        nCols(_nCols), nBlocksPerRow(_nRows / _rowsPerBlock),
        nBlocksPerCol(_nCols / _colsPerBlock) {
    Blocks = std::vector<std::vector<float *>>(nBlocksPerCol);
    for (int i = 0; i < nBlocksPerCol; i++) {
      std::vector<float *> rowBlocks(nBlocksPerRow);
      for (int j = 0; j < nBlocksPerRow; j++) {
        rowBlocks[j] =
            (float *)calloc(_rowsPerBlock * _colsPerBlock, sizeof(float));
      }
      Blocks[i] = rowBlocks;
    }
  };

  ~BlockMatrix() {};

  // Initialize the BlockMatrix from 2D arrays
  void Initialize(float *matrix) {
    fprintf(stdout, "nBlocksPerRow %d\n", nBlocksPerRow);
    fprintf(stdout, "nBlocksPerCol %d\n", nBlocksPerCol);
    for (int i = 0; i < nBlocksPerCol; i++)
      for (int j = 0; j < nBlocksPerRow; j++) {
        fprintf(stdout, "GETBLOCK %d,%d\n", i, j);
        float *CurrBlock = GetBlock(i, j);
        // fprintf(stdout, "ColumnPerBlocks = %d\n", colsPerBlock);
        for (int ii = 0; ii < colsPerBlock; ++ii)
          for (int jj = 0; jj < rowsPerBlock; ++jj) {
            int curri = i * colsPerBlock + ii;
            int currj = j * rowsPerBlock + jj;

            // fprintf(stdout, "I,J = %d,%d\n", curri, currj);
            // fprintf(stdout, "MAT[I][J] = %f\n", matrix[curri + currj]);
            CurrBlock[ii + jj * colsPerBlock] = matrix[curri + currj * nCols];
            // fprintf(stdout, "i,j / ii,jj = %d,%d / %d,%d\n", i, j, ii, jj);
            // fprintf(stdout, "curri,currj  = %d,%d \n", curri, currj);
            // fprintf(stdout, "BlockMAT[%d][%d] = %f\n", ii, jj, CurrBlock[ii
            // + jj* colsPerBlock]); fprintf(stdout, "MAT[%d][%d] = %f\n",
            // curri, currj, matrix[curri *nBlocksPerCol + currj]);
          }
      }
  }

  long Compare(float *matrix) {
    long fail=0;
    for (int i = 0; i < nBlocksPerCol; i++)
      for (int j = 0; j < nBlocksPerRow; j++) {
        //fprintf(stdout, "GETBLOCK %d,%d\n", i, j);
        float *CurrBlock = GetBlock(i, j);
        //fprintf(stdout, "ColumnPerBlocks = %d\n", colsPerBlock);
        for (int ii = 0; ii < colsPerBlock; ++ii)
          for (int jj = 0; jj < rowsPerBlock; ++jj) {
            int curri = i * colsPerBlock + ii;
            int currj = j * rowsPerBlock + jj;

            float m_value = matrix[curri + currj * nCols];
            float bm_value = CurrBlock[ii + jj  * colsPerBlock];

            if(bm_value != m_value){
              fprintf(stdout, "i,j = %d,%d\n", i, j);
              fprintf(stdout, "BlockMAT[%d][%d] = %f\n", ii, jj, bm_value);
              fprintf(stdout, "MAT[%d][%d] = %f\n", curri, currj, m_value);
              fail++;
            }
          }
      }

    // Print results
    printf("Non-Matching Block Outputs: %d\n", fail);
    return fail;
  }

  float *GetBlock(int i, int j) {
    assert(i < nBlocksPerCol && j < nBlocksPerRow && "Accessing outside block");
    return Blocks[i][j];
  }
};

#define SIZE 1024

#define BS 256
#define N SIZE

#define PERCENT_DIFF_ERROR_THRESHOLD 0.01

// Initialize matrices.
void init(float *a, float *b, float *c_cpu, float *c_gpu) {
  int i, j;
  for (i = 0; i < SIZE; ++i) {
    for (j = 0; j < SIZE; ++j) {
      a[i * SIZE + j] = (float)i + j % 100;
      b[i * SIZE + j] = (float)i + j % 100;
      c_cpu[i * SIZE + j] = 0.0f;
      c_gpu[i * SIZE + j] = 0.0f;
      // printf("a[%d][%d] = %f\n", i, j, a[i * SIZE + j]);
      // printf("b[%d][%d] = %f\n", i, j, b[i * SIZE + j]);
    }
  }
}

int OldBlockMatMul(float **A, float **B, float **C) {
  //#pragma omp target device(GPU_CLUSTER)
  int i, j, k;
  for (i = 0; i < N; i += BS)
    for (j = 0; j < N; j += BS)
      for(k = 0; k < N; k+=BS)
        #pragma omp task depend(in: A[i:BS][k:BS], B[k:BS][j:BS]) depend(inout: C[i:BS][j:BS])
        //#pragma omp target map(to: A[:N*N], B[:N*N]) map(tofrom: C[:N*N])
        #pragma omp parallel for
        for (int ii = i; ii < i + BS; ii++)
          for (int jj = j; jj < j + BS; jj++) {
            C[ii][jj] = 0;
            for (int kk = k; kk < k + BS; ++k)
              C[ii][jj] += A[ii][kk] * B[kk][jj];
          }
  return 0;
}

int BlockMatMul(BlockMatrix &A, BlockMatrix &B, BlockMatrix &C) {
  float *BlockA, *BlockB, *BlockC;
  for (int i = 0; i < N / BS; ++i)
    for (int j = 0; j < N / BS; ++j) {
      BlockC = C.GetBlock(i, j);
      for (int k = 0; k < N / BS; ++k) {
        BlockA = A.GetBlock(i, k);
        BlockB = B.GetBlock(k,j);
        #pragma omp target depend(in: BlockA, BlockB) depend(inout: BlockC) \
                           map(to: BlockA[:BS*BS], BlockB[:BS*BS]) map(tofrom: BlockC[:BS*BS]) nowait
        #pragma omp parallel for
        for(int ii = 0; ii < BS; ii++)
          for(int jj = 0; jj < BS; jj++) {
            for(int kk = 0; kk < BS; ++kk)
              BlockC[ii + jj * BS] += BlockA[ii + kk * BS] * BlockB[kk + jj * BS];
          }
      }
    }
  return 0;
}

/// matrix multiplication algorithm GPU
void mul_GPU(float *a, float *b, float *c) {
#pragma omp target map(to : a[:SIZE*SIZE], b[:SIZE*SIZE]) map(tofrom : c[ : SIZE *SIZE])
#pragma omp parallel for
  for (int i = 0; i < SIZE; ++i) {
    for (int j = 0; j < SIZE; ++j) {
      float sum = 0.0;
      for (int k = 0; k < SIZE; ++k) {
        sum += a[i * SIZE + k] * b[k * SIZE + j];
      }
      c[i * SIZE + j] = sum;
    }
  }
}

void mul_CPU(float *a, float *b, float *c) {
  for (int i = 0; i < SIZE; ++i) {
    for (int j = 0; j < SIZE; ++j) {
      float sum = 0.0;
      for (int k = 0; k < SIZE; ++k) {
        sum = sum + a[i * SIZE + k] * b[k * SIZE + j];
      }
      c[i * SIZE + j] = sum;
    }
  }
}

int compareResults(float *b_cpu, float *b_gpu) {
  int fail = 0;

  for (int i = 0; i < SIZE; i++) {
    for (int j = 0; j < SIZE; j++) {
      if (b_cpu[i * SIZE + j] != b_gpu[i * SIZE + j]) {
        fail++;
        if (i < 10)
          fprintf(stdout, "%f != %f \n", b_cpu[i * SIZE + j],
                  b_gpu[i * SIZE + j]);
      }
    }
  }

  // Print results
  printf("Non-Matching CPU-GPU Outputs: %d\n", fail);
  return fail;
}

int main(int argc, char *argv[]) {

  // double t_start, t_end;
  int fail = 0;
  float *a, *b, *c_cpu, *c_gpu;

  a = (float *)malloc(sizeof(float) * SIZE * SIZE);
  b = (float *)malloc(sizeof(float) * SIZE * SIZE);
  c_cpu = (float *)calloc(sizeof(float), SIZE * SIZE);
  c_gpu = (float *)calloc(sizeof(float), SIZE * SIZE);

  auto BlockedA = BlockMatrix(BS, BS, N, N);
  auto BlockedB = BlockMatrix(BS, BS, N, N);
  auto BlockedC = BlockMatrix(BS, BS, N, N);

  fprintf(stdout, "<< Init Matrices >>\n");

  init(a, b, c_cpu, c_gpu);

  printf("a[0] = %f\n", a[0]);

  BlockedA.Initialize(a);
  BlockedB.Initialize(b);

  fprintf(stdout, "<< Matrix Multiplication >>\n");

  // t_start = rtclock();
  mul_GPU(a, b, c_gpu);
  // t_end = rtclock();
  fprintf(stdout, "GPU Computation done\n");

  // t_start = rtclock();
  mul_CPU(a, b, c_cpu);
  // t_end = rtclock();
  fprintf(stdout, "CPU Computation done\n");

  fail = compareResults(c_cpu, c_gpu);

  BlockMatMul(BlockedA, BlockedB, BlockedC);

  fprintf(stdout, "Block Computation done\n");

  BlockedA.Compare(a);
  BlockedB.Compare(b);
  BlockedC.Compare(c_cpu);

  free(a);
  free(b);
  free(c_cpu);
  free(c_gpu);

  return fail;
}
