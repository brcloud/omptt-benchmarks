
// Required precision in float comparisons
#define EPSILON 0.0000001

long idx(long size, long i, long j, long k);
long idx(long size, long i, long j);

long int mod(long int a, long int b);

void compare_matrices(float *a, float *b, long int n);
void print_array(float *a, long int n);
void print_array(float *a, long int row, long int col);
void print_speedup(double ss, double se, double ps, double pe);
