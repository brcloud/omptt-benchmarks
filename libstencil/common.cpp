#include <cmath>
#include <cstdio>

#include "common.h"

////////////////////////////////////////////////////////////////////////////////
// LINEAR MATRIX UTILITIES
#pragma omp declare target
// return the index equivalent to matrix[i][j][k]
long idx(long size, long i, long j, long k){
  return ((i * size * size) + (j * size) + k);
}
// return the index equivalent to matrix[i][j]
long idx(long size, long i, long j){
  return ((i * size) + j);
}
#pragma omp end declare target


////////////////////////////////////////////////////////////////////////////////
// MATH

// supports mod of negative numbers
long int mod(long int a, long int b)
{
    long int r = a % b;
    return r < 0 ? r + b : r;
}

////////////////////////////////////////////////////////////////////////////////
// GENERAL

void compare_matrices(float *a, float *b, long int n){
  long int count = 0;
  for(int i = 0; i < n; i++){
    if (fabs(a[i] - b[i]) > EPSILON){
      count++;
    }
  }
  printf("Number of non-matching elements: %ld\n", count);
}

// Pretty prints as array
void print_array(float *a, long int n){
  for (long i = 0; i < n; i++)
      printf("%.2f ", a[i]);
  printf("\n");
}

// Pretty prints as 2D Matrix
void print_array(float *a, long int row, long int col){
  for (long i = 0; i < row; i++){
    for (long j = 0; j < col; j++)
      printf("%.2f ", a[i * col + j]);
    printf("\n");
  }
  printf("\n");
}

void print_speedup(double serial_start, double serial_end, double par_start, double par_end){
    printf("speedup: %.2lf\n", (serial_end - serial_start) / (par_end - par_start));
}
