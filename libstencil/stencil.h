/*
  CONSTANTS

    USER-DEFINED
    SIZE: the input matrix will be SIZE * SIZE.
    STEPS: no of iterations to compute the stencil.
    SPACE: quantity of scratch space. Must be greater than 1.
    PADDING: problem-specific padding.
    BLOCK_FACTOR: specifies how many non-padding elements will be in each block.
                  Each block will have BLOCK_FACTOR^2 such elements.
                  SIZE - 2 * PADDING must be divisible by BLOCK_FACTOR.

    IMPLEMENTATION-DEFINED
    BSIZE: each block contains BSIZE * BSIZE cells. Computed as (FACTOR + 2 * PADDING).
    BLOCKS: total no of blocks. Computed as ((SIZE - 2 * PADDING)/FACTOR)^2
    LAST_IDX: index of the last non-padding element of a row/col.

*/

#define SIZE 10
#define STEPS 100
#define SPACE 5
#define PADDING 1
#define BLOCK_FACTOR 4
#define NODES 4

// KERNEL
#ifdef KERNEL
	void KERNEL(float *blocks, float *scratch);
#else
	#define KERNEL foo
#endif

// NOT USER-DEFINED
#define BSIZE (BLOCK_FACTOR + 2 * PADDING)
#define BLOCKS_PER_LINE (SIZE - 2 * PADDING) / BLOCK_FACTOR
#define BLOCKS (BLOCKS_PER_LINE * BLOCKS_PER_LINE)
#define LAST_IDX (SIZE - 1 - PADDING)


// KERNEL SIGNATURE



// SIGNATURES
float **get_blocks();
void split_stencil(float **blocks, float *in);
void stitch_stencil(float **blocks, float *in);
void solve_stencil(float **blocks, float *halo);
float* init_halo_map(float **blocks);

float* get_handler(int i, int j, float *pads);
void fill_padding(float *out_pad, float *block);
void set_padding(float *block, long i, long j, float *pad);

void print_blocks(float **blocks);
void print_halo_map(float *h);
