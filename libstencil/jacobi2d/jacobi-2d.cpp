#include <cstdio>
#include <cstdlib>
#include <cmath>

#include <omp.h>

#include "../common.h"
#include "../stencil.h"

using namespace std;


#pragma omp declare target

// void jacobi(float *a, long int block_size, int in_idx, int out_idx){
//   #pragma omp parallel for collapse(2) schedule(static, 1)
//   for (long int i = 1; i < block_size - 1; i++){
//     for (long int j = 1; j < block_size - 1; j++){
//       a[idx(block_size, out_idx, i, j)] = 0.2 * (a[idx(block_size, in_idx, i, j)] +
//                                                  a[idx(block_size, in_idx, i, j-1)] +
//                                                  a[idx(block_size, in_idx, i, j+1)] +
//                                                  a[idx(block_size, in_idx, i+1, j)] +
//                                                  a[idx(block_size, in_idx, i-1, j)]);
//     }
//   }
// }

// simplified kernel
void jacobi(float *block, float *scratch){
  long int block_size = BSIZE;

  // stencil
  #pragma omp parallel for collapse(2) schedule(static, 1)
  for (long int i = 1; i < block_size - 1; i++){
    for (long int j = 1; j < block_size - 1; j++){
      scratch[idx(block_size, i, j)] = 0.2 * (block[idx(block_size, i, j)] +
                                              block[idx(block_size, i, j-1)] +
                                              block[idx(block_size, i, j+1)] +
                                              block[idx(block_size, i+1, j)] +
                                              block[idx(block_size, i-1, j)]);
    }
  }

  // copy-back
  #pragma omp parallel for collapse(2) schedule(static, 1)
  for (long int i = 1; i < block_size - 1; i++)
    for (long int j = 1; j < block_size - 1; j++)
      block[idx(block_size, i, j)] = scratch[idx(block_size, i, j)];

}

#pragma omp end declare target

int main(int argc, char *argv[]){

  //////////////////////// SERIAL ////////////////////////

  double s_start, s_end;
  s_start = omp_get_wtime();

  // allocate arrays
  float *A = (float*) malloc(sizeof(float) * SIZE * SIZE);
  float *B = (float*) malloc(sizeof(float) * SIZE * SIZE);

  // intializing arrays w/ input
  for (long int i = 0; i < SIZE; i++){
    for (long int j = 0; j < SIZE; j++){
      A[idx(SIZE, i, j)] = sqrt(i * SIZE + j);
      B[idx(SIZE, i, j)] = 0;
    }
  }

  // DEBUG
  // print_array(A, SIZE, SIZE);

  // stencil
  long int t, i, j;
  for (t = 0; t < STEPS; t++){

    // calc
    for (i = 1; i < SIZE - 1; i++)
      for (j = 1; j < SIZE - 1; j++)
        B[idx(SIZE, i, j)] = 0.2 * (A[idx(SIZE, i, j)] +
                                    A[idx(SIZE, i, j-1)] +
                                    A[idx(SIZE, i, j+1)] +
                                    A[idx(SIZE, i+1, j)] +
                                    A[idx(SIZE, i-1, j)]);

    // copy back
    for (i = 1; i < SIZE - 1; i++)
      for (j = 1; j < SIZE - 1; j++)
        A[idx(SIZE, i, j)] = B[idx(SIZE, i, j)];

  }

  s_end = omp_get_wtime();

  //////////////////////// PARALLEL ////////////////////////

  printf("Starting Parallel\n");
  double p_start, p_end;
  p_start = omp_get_wtime();

  //  generating input
  float *c = (float*) malloc(sizeof(float) * SIZE * SIZE);
  for (long int i = 0; i < SIZE; i++){
    for (long int j = 0; j < SIZE; j++){
      c[idx(SIZE, i, j)] = sqrt(i * SIZE + j);
    }
  }

  // allocs the blocks
  float **blocks = get_blocks();

  // splits the input into blocks
  split_stencil(blocks, c);

  // init the halo DS and get the initial haloes
  float* halo = init_halo_map(blocks);

  // does the actual computation
  solve_stencil(blocks, halo);

  // copying the elements from each block into the output matrix
  stitch_stencil(blocks, c);

  // TODO: add a library function to dealloc memory
  // free_stencil();

  p_end = omp_get_wtime();

	//////////////////////// OUTPUT ////////////////////////

  // print_array(A, SIZE, SIZE);
  // print_array(c, SIZE, SIZE);
	compare_matrices(A, c, SIZE * SIZE);
	print_speedup(s_start, s_end, p_start, p_end);

  // deallocate arrays
  free(A);
  free(B);
  free(c);

  return 0;
}
