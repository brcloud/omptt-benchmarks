#include <cstdlib>
#include <cstdio>

#include "stencil.h"
#include "common.h"

//  TODO: add constant space optimization


long pad_idx(long int i, long int j, int border){
  return (i * BLOCKS_PER_LINE * 4 * BSIZE) + (j * 4 * BSIZE) + (border * BSIZE);
}

/////// dummy kernel
void foo(float *blocks, float *scratch){
  return;
}

////////////////////////////////////////////////////////////////////////////////
// Initialization

// alloc array of blocks
float** get_blocks(){
  float **blocks = (float**) malloc(sizeof(float*) * BLOCKS);

  for (int i = 0; i < BLOCKS; i++){
    // blocks[i] = (float*) malloc(sizeof(float) * BSIZE * BSIZE * SPACE);
    blocks[i] = (float*) malloc(sizeof(float) * BSIZE * BSIZE);
  }

  return blocks;
}

// dealloc array of blocks
void free_blocks(float **blocks){
  for (long i = 0; i < BLOCKS; i++)
    free(blocks[i]);

  free(blocks);
}

  // split the input into blocks
void split_stencil(float **blocks, float *in){

  float *block;
  long int start_i = 0, start_j = 0;

  for (long int b = 0; b < BLOCKS; b++){

    block = blocks[b];

    // copying the input to block b
    for (long int i = 0; i < BSIZE; i++){
      for (long int j = 0; j < BSIZE; j++){
        block[i * BSIZE + j] = in[idx(SIZE, start_i + i, start_j + j)];
      }
    }

    // setting the start position for the next block
    start_j += BLOCK_FACTOR;
    if (start_j == LAST_IDX){
      start_j = 0;
      start_i += BLOCK_FACTOR;
    }
  } // for
} // split_stencil

void stitch_stencil(float **blocks, float *in){

  float *block;
  long start_i = 0, start_j = 0;

  for (long int b = 0; b < BLOCKS; b++){

    block = blocks[b];

    // copying the input to block b
    for (long int i = 1; i < BSIZE - 1; i++){
      for (long int j = 1; j < BSIZE - 1; j++){
        in[idx(SIZE, start_i + i, start_j + j)] = block[i * BSIZE + j];
      }
    }

    // setting the start position for the next block
    start_j += BLOCK_FACTOR;
    if (start_j == LAST_IDX){
      start_j = 0;
      start_i += BLOCK_FACTOR;
    }
  } // for
}

// init the padding data structure
float* init_halo_map(float **blocks){

  float *block;
  float *pad = (float*) malloc(sizeof(float) * BLOCKS * 4 * BSIZE);

  for (int b = 0; b < BLOCKS; b++){
    block = blocks[b];
    for (int i = 0; i < BSIZE; i++){
      pad[b * 4 * BSIZE + 0 * BSIZE + i] = block[i + BSIZE]; // second row
      pad[b * 4 * BSIZE + 1 * BSIZE + i] = block[(BSIZE - 2) + (BSIZE * i)];
      pad[b * 4 * BSIZE + 2 * BSIZE + i] = block[i + (BSIZE * (BSIZE - 2))];
      pad[b * 4 * BSIZE + 3 * BSIZE + i] = block[1 + BSIZE * i];
    }
  }

  return pad;
}

////////////////////////////////////////////////////////////////////////////////

void solve_stencil(float **blocks, float *halo){

  // create and alloc a scratch space for the simplified kernel
  float *scratch = (float*) calloc(BSIZE * BSIZE, sizeof(float));
  #pragma omp target enter data map(alloc: scratch[:BSIZE * BSIZE])

  float *out_pad;
  float *block;

  for (int z = 0; z < STEPS; z++){
    // updating all paddings on host
    for (long int i = 0; i < BLOCKS_PER_LINE; i++){
      for (long int j = 0; j < BLOCKS_PER_LINE; j++){
        block = blocks[i * BLOCKS_PER_LINE + j];     // handle to block (i, j)
        set_padding(block, i, j, halo);
        // print_array(block, BSIZE, BSIZE);
      }
    }

    // stencil calculation
    for (long int i = 0; i < BLOCKS_PER_LINE; i++){
      for (long int j = 0; j < BLOCKS_PER_LINE; j++){

        block = blocks[i * BLOCKS_PER_LINE + j]; // handle to block (i, j)

        // get pointers to write my out paddings and map them to device
        out_pad = get_handler(i, j, halo);
        #pragma omp target enter data map(to: out_pad[:BSIZE * 4])

        // map the block and run the kernel
        #pragma omp target enter data map(to: block[:BSIZE * BSIZE])
        #pragma omp target
        {
          // (&jacobi)(block, scratch);
          KERNEL(block, scratch);
          fill_padding(out_pad, block);
        }

        // send the block and the out paddings back to device
        #pragma omp target exit data map(from: out_pad[:BSIZE * 4])
        #pragma omp target exit data map(from: block[:BSIZE * BSIZE])

      }
    }
  } // for z < STEPS

  #pragma omp target exit data map(delete: scratch[:BSIZE * BSIZE])

} // solve_stencil


/*
  Given the padding data structure, this function sets the proper paddings to
  the block (i, j)
*/
void set_padding(float *block, long i, long j, float *pad){
  float *base;

  // my upper pad
  if (i - 1 >= 0){
    base = pad + pad_idx(i - 1, j, 2);
    for (long int i = 0; i < BSIZE; i++)
      block[i] = base[i];
  }

  // my right pad
  if (j + 1 < BLOCKS_PER_LINE){
    base = pad + pad_idx(i, j + 1, 3);
    for (long int i = 0; i < BSIZE; i++)
      block[(i * BSIZE) + BSIZE - 1] = base[i];
      // block[(i * BSIZE) + BSIZE - 1] = 7;
  }

  // my lower pad
  if (i + 1 < BLOCKS_PER_LINE){
    base = pad + pad_idx(i + 1, j, 0);
    for (long int i = 0; i < BSIZE; i++)
      block[(BSIZE - 1) * BSIZE + i] = base[i];
  }

  // my left pad
  if (j - 1 >= 0){
    base = pad + pad_idx(i, j - 1, 1);
    for (long int i = 0; i < BSIZE; i++)
      block[BSIZE * i] = base[i];
  }

}

/*
  Gets the pointer within the padding data structure where each block should
  write its output paddings.
*/
float* get_handler(int i, int j, float *pads){
  return pads + (i * BLOCKS_PER_LINE * 4 * BSIZE) + (j * 4 * BSIZE);
}

#pragma omp declare target
/*
  Given a block where the stencil is done, this function fills the paddings
  that will be sent back to host. The positions where to look for data depends
  on the padding thickness.

  TODO: change this for any padding thickness
*/
void fill_padding(float *out_pad, float *block){
  for (int i = 0; i < BSIZE; i++){
    out_pad[i] = block[BSIZE + i];                             // the second row
    out_pad[1 * BSIZE + i] = block[(BSIZE - 2) + (BSIZE * i)]; // penultimate column
    out_pad[2 * BSIZE + i] = block[i + (BSIZE * (BSIZE - 2))]; // penultimate row
    out_pad[3 * BSIZE + i] = block[1 + BSIZE * i];             // second col
  }
}
#pragma omp end declare target



////////////////////////////////////////////////////////////////////////////////
// DEBUG

void print_blocks(float **blocks){
  for (long b = 0; b < BLOCKS; b++){
    print_array(blocks[b], BSIZE, BSIZE);
  }
}

// DEBUG: check the paddings
void print_halo_map(float *h){
  for (int b = 0; b < BLOCKS; b++){
    for (int j = 0; j < 4; j++){
      print_array(h + b * 4 * BSIZE + j * BSIZE, BSIZE);
    }
    printf("\n");
  }
}




///// TODO: turn this snippet into a test for set_padding()

// // DEBUG: test the set_padding function
// for (long int i = 0; i < BLOCKS_PER_LINE; i++){
//   for (long int j = 0; j < BLOCKS_PER_LINE; j++){
//     float *tblock = (float*) calloc(BSIZE * BSIZE, sizeof(float));
//     set_padding(tblock, i, j, pad);
//     print_array(tblock, BSIZE, BSIZE);
//   }
// }
